/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.10
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QLabel *httpClientLabel;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *methodLabel;
    QComboBox *methodComboBox;
    QLabel *label;
    QHBoxLayout *horizontalLayout_2;
    QLabel *urlLabel;
    QLineEdit *urlField;
    QPushButton *submitButton;
    QPushButton *clearButton;
    QLabel *payloadLabel;
    QPlainTextEdit *payloadField;
    QLabel *responseLabel;
    QPlainTextEdit *responseTextEdit;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(806, 600);
        MainWindow->setStyleSheet(QString::fromUtf8(""));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        httpClientLabel = new QLabel(centralwidget);
        httpClientLabel->setObjectName(QString::fromUtf8("httpClientLabel"));
        httpClientLabel->setBaseSize(QSize(0, 0));
        QFont font;
        font.setPointSize(18);
        font.setBold(true);
        httpClientLabel->setFont(font);
        httpClientLabel->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(httpClientLabel);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        methodLabel = new QLabel(centralwidget);
        methodLabel->setObjectName(QString::fromUtf8("methodLabel"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(methodLabel->sizePolicy().hasHeightForWidth());
        methodLabel->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(methodLabel);

        methodComboBox = new QComboBox(centralwidget);
        methodComboBox->addItem(QString());
        methodComboBox->addItem(QString());
        methodComboBox->setObjectName(QString::fromUtf8("methodComboBox"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(methodComboBox->sizePolicy().hasHeightForWidth());
        methodComboBox->setSizePolicy(sizePolicy1);

        horizontalLayout->addWidget(methodComboBox);


        verticalLayout->addLayout(horizontalLayout);

        label = new QLabel(centralwidget);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font1;
        font1.setBold(true);
        label->setFont(font1);

        verticalLayout->addWidget(label);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        urlLabel = new QLabel(centralwidget);
        urlLabel->setObjectName(QString::fromUtf8("urlLabel"));

        horizontalLayout_2->addWidget(urlLabel);

        urlField = new QLineEdit(centralwidget);
        urlField->setObjectName(QString::fromUtf8("urlField"));

        horizontalLayout_2->addWidget(urlField);

        submitButton = new QPushButton(centralwidget);
        submitButton->setObjectName(QString::fromUtf8("submitButton"));
        submitButton->setAutoFillBackground(true);
        submitButton->setFlat(false);

        horizontalLayout_2->addWidget(submitButton);

        clearButton = new QPushButton(centralwidget);
        clearButton->setObjectName(QString::fromUtf8("clearButton"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(100);
        sizePolicy2.setHeightForWidth(clearButton->sizePolicy().hasHeightForWidth());
        clearButton->setSizePolicy(sizePolicy2);
        clearButton->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout_2->addWidget(clearButton);


        verticalLayout->addLayout(horizontalLayout_2);

        payloadLabel = new QLabel(centralwidget);
        payloadLabel->setObjectName(QString::fromUtf8("payloadLabel"));
        QSizePolicy sizePolicy3(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(payloadLabel->sizePolicy().hasHeightForWidth());
        payloadLabel->setSizePolicy(sizePolicy3);

        verticalLayout->addWidget(payloadLabel);

        payloadField = new QPlainTextEdit(centralwidget);
        payloadField->setObjectName(QString::fromUtf8("payloadField"));

        verticalLayout->addWidget(payloadField);

        responseLabel = new QLabel(centralwidget);
        responseLabel->setObjectName(QString::fromUtf8("responseLabel"));
        responseLabel->setFont(font1);

        verticalLayout->addWidget(responseLabel);

        responseTextEdit = new QPlainTextEdit(centralwidget);
        responseTextEdit->setObjectName(QString::fromUtf8("responseTextEdit"));
        responseTextEdit->setEnabled(true);

        verticalLayout->addWidget(responseTextEdit);

        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "HTTP Client", nullptr));
        httpClientLabel->setText(QCoreApplication::translate("MainWindow", "HTTP Client", nullptr));
        methodLabel->setText(QCoreApplication::translate("MainWindow", "Method:", nullptr));
        methodComboBox->setItemText(0, QCoreApplication::translate("MainWindow", "GET", nullptr));
        methodComboBox->setItemText(1, QCoreApplication::translate("MainWindow", "POST", nullptr));

        label->setText(QCoreApplication::translate("MainWindow", "Request:", nullptr));
        urlLabel->setText(QCoreApplication::translate("MainWindow", "URL:", nullptr));
        submitButton->setText(QCoreApplication::translate("MainWindow", "Submit", nullptr));
        clearButton->setText(QCoreApplication::translate("MainWindow", "Clear", nullptr));
        payloadLabel->setText(QCoreApplication::translate("MainWindow", "Payload:", nullptr));
        responseLabel->setText(QCoreApplication::translate("MainWindow", "Response:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
