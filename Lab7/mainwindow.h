#pragma once
#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QByteArray>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_methodComboBox_currentTextChanged();
    void on_clearButton_released();
    void on_submitButton_released();
    void networkRequestFinished(QNetworkReply* response);

private:
    QNetworkAccessManager* networkManager;
    Ui::MainWindow *ui;

    const QString GET_METHOD = "GET";
    const QString POST_METHOD = "POST";

    bool isGetMethod(const QString& method);
    bool isPostMethod(const QString& method);
    void hidePayloadFields();
    void showPayloadFields();
    void selectAppropriateMode();
};
