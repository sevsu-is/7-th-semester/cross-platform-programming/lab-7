#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>

void MainWindow::hidePayloadFields() {
    ui->payloadField->hide();
    ui->payloadLabel->hide();
}

void MainWindow::showPayloadFields() {
    ui->payloadField->show();
    ui->payloadLabel->show();
}

void MainWindow::selectAppropriateMode() {
    QString method = ui->methodComboBox->currentText();
    if (isGetMethod(method)) {
        hidePayloadFields();
        ui->urlField->setText("https://api.open-meteo.com/v1/forecast?latitude=44.608&longitude=33.5213&hourly=temperature_2m&timezone=Europe%2FMoscow");
        ui->urlField->setCursorPosition(0);
    } else if (isPostMethod(method)) {
        showPayloadFields();
        ui->urlField->setText("http://api.forismatic.com/api/1.0/");
        ui->urlField->setCursorPosition(0);
        ui->payloadField->setPlainText("method=getQuote&key=457653&format=xml&lang=ru");
    }
    ui->responseTextEdit->setReadOnly(true);
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    selectAppropriateMode();
    networkManager = new QNetworkAccessManager(this);
    connect(networkManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(networkRequestFinished(QNetworkReply*)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::isGetMethod(const QString& method) {
    return QString::compare(method, GET_METHOD, Qt::CaseInsensitive) == 0;
}

bool MainWindow::isPostMethod(const QString& method) {
    return QString::compare(method, POST_METHOD, Qt::CaseInsensitive) == 0;
}

void MainWindow::on_methodComboBox_currentTextChanged()
{
    selectAppropriateMode();
}

void MainWindow::on_clearButton_released()
{
    ui->urlField->setText("");
    ui->payloadField->setPlainText("");
    ui->responseTextEdit->setPlainText("");
}

void MainWindow::on_submitButton_released()
{
    QString method = ui->methodComboBox->currentText();
    QString url = ui->urlField->text();
    QByteArray payload = ui->payloadField->toPlainText().toUtf8();

    if (isGetMethod(method)) {
        networkManager->get(QNetworkRequest(QUrl(url)));
    } else if (isPostMethod(method)) {
        networkManager->post(QNetworkRequest(QUrl(url)), payload);
    }
}

void MainWindow::networkRequestFinished(QNetworkReply* networkReply) {
    if (networkReply->error() == QNetworkReply::NoError) {
        QByteArray responseBody = networkReply->readAll();
        ui->responseTextEdit->setPlainText(responseBody);
    } else {
        QMessageBox::critical(this, tr("Network communication error"), networkReply->errorString());
        return;
    }
}
